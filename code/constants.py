"""
Author: Benedikt Lotter
Project-wide definition of constants / globals / variables to be imported by other scripts
"""

"""
Input data location parameters
"""
DATA_DIR = "../data/"
MODELS_DIR = "../models/"
RESULTS_DIR = "../output/results/"
PLOT_DIR = "../output/plot/"
ROC_DIR = "../output/plot/roc/"
REPORTS_DIR = "../output/reports/"
TABLES_DIR = "../output/tables/"
LATEX_TABLES_DIR = "../output/latex/tables/"

CV_RESULTS_BASE_FN = "actual_pred_per_beta"
MODEL_BASE_FN = "model"
PREDICTIONS_BASE_FN = "predictions"
TRAIN_TEST_SPLIT = "train_test_split"

INDEX_TO_X_VAR_MAP = DATA_DIR + "index_to_x_var.pkl"

#Merged set of loans across years
ALL_DATA = "loans_ALL.pkl"
RPP_DATA = DATA_DIR + 'rpp_by_state.csv'
CLASSIFIERS = ["MLR", "SVM"]
MODEL_TYPES = ["DEFAULT_CLASSIFY", "RECOV_REGR"]
"""
Credit Metrics
"""
GRADES = ["A", "B", "C", "D", "E", "F", "G"]
ALL_GRADES = "ALL"
GRADES_CONCAT = ["A", "B", "C", "D", "E", "F", "G", "ALL"]


"""
Data pre-processing params
"""

RPP_ADJ_VARS = ["loan_amnt",
                "installment",
                "annual_inc",
                "open_acc",
                "revol_bal",
                "out_prncp",
                "out_prncp_inv",
                "total_pymnt",
                "total_pymnt_inv",
                "total_rec_prncp",
                "total_rec_int",
                "total_rec_late_fee",
                "recoveries",
                "collection_recovery_fee",
                "last_pymnt_amnt"]


GOOD_STATUS = ['Fully Paid'] + ['Does not meet the credit policy.  Status:Fully Paid']
BAD_STATUS = ['Default', 'Charged Off'] + ['Does not meet the credit policy.  Status:Charged Off']
DONE_STATUS = GOOD_STATUS+BAD_STATUS

PERCENTAGE_COLUMNS = ['int_rate', 'revol_util']

COLUMNS_TO_DROP = ['member_id', 'application_type', 'annual_inc_joint', 'dti_joint', 'verification_status_joint', 'last_credit_pull_d', 'next_pymnt_d', 'mths_since_last_delinq', 'mths_since_last_record', 'mths_since_last_major_derog', 'inq_last_6mths', 'last_fico_range_high', 'last_fico_range_low', 'last_pymnt_d', 'last_pymnt_amnt', 'out_prncp', 'out_prncp_inv', 'purpose', 'pymnt_plan', 'recoveries', 'title', 'url', 'zip_code', 'addr_state', 'funded_amnt', 'funded_amnt_inv', 'emp_title', 'desc', 'earliest_cr_line', 'fico_range_low', 'fico_range_high', 'collection_recovery_fee', 'collections_12_mths_ex_med']

NON_NA_COLUMNS = ['int_rate', 'revol_util', 'total_acc', 'pub_rec', 'open_acc', 'delinq_2yrs', 'annual_inc']

DISCRETE_COLUMNS = ['sub_grade', 'home_ownership', 'initial_list_status', 'verification_status', 'policy_code', 'term']

"""
Data structure / meta data parameters
"""
Y_VAR_CLASSIFY = "loan_status_binary"
Y_VAR_RECOV_REGR = "recovery_rate"


X_VARS_NUM = ["loan_amnt",
              "installment",
              "int_rate",
              "int_premium",
              "dti",
              "emp_length_num",
              "annual_inc",
              "open_acc",
              "total_acc",
              "loan_issue_volume",
              "loan_issue_volume_dev",
              "loan_issue_volume_grade",
              "loan_issue_volume_grade_dev"
              ]

X_VARS_CAT = ["sub_grade",
              "home_ownership",
              "verification_status",
              "term",
              "initial_list_status"]

X_VARS_CAT_REGR = ["sub_grade"]

X_VARS_REGR = ["loan_amnt",
              "installment",
              "int_rate",
              "int_premium",
              "dti",
              "emp_length_num",
              "annual_inc",
              "open_acc",
              "total_acc"
              ]



TEST_PRED_COLUMNS = ['test_ids', 'y_test_actual', 'y_test_pred', 'y_test_prob']


"""
Output file locations
"""
OUT_DIR = "../output/"
SVM_ROC = OUT_DIR + "svm_roc_plot.pdf"
OUT_RES_BAL_PKL = ["actual_pred_per_beta_MLR_A.pkl",
                   "actual_pred_per_beta_MLR_B.pkl",
                   "actual_pred_per_beta_MLR_C.pkl",
                   "actual_pred_per_beta_MLR_D.pkl",
                   "actual_pred_per_beta_MLR_E.pkl",
                   "actual_pred_per_beta_MLR_F.pkl",
                   "actual_pred_per_beta_MLR_G.pkl",
                   "actual_pred_per_beta_SVM_A.pkl",
                   "actual_pred_per_beta_SVM_B.pkl",
                   "actual_pred_per_beta_SVM_C.pkl",
                   "actual_pred_per_beta_SVM_D.pkl",
                   "actual_pred_per_beta_SVM_E.pkl",
                   "actual_pred_per_beta_SVM_F.pkl",
                   "actual_pred_per_beta_SVM_G.pkl",
                   "actual_pred_per_beta_MLR_ALL.pkl",
                   "actual_pred_per_beta_SVM_ALL.pkl"]
OUT_REPORTS = ["reports_accross_betas_MLR_A.pkl",
               "reports_accross_betas_MLR_B.pkl",
               "reports_accross_betas_MLR_C.pkl",
               "reports_accross_betas_MLR_D.pkl",
               "reports_accross_betas_MLR_E.pkl",
               "reports_accross_betas_MLR_F.pkl",
               "reports_accross_betas_MLR_G.pkl",
               "reports_accross_betas_MLR_ALL.pkl",
               "reports_accross_betas_SVM_A.pkl",
               "reports_accross_betas_SVM_B.pkl",
               "reports_accross_betas_SVM_C.pkl",
               "reports_accross_betas_SVM_D.pkl",
               "reports_accross_betas_SVM_E.pkl",
               "reports_accross_betas_SVM_F.pkl",
               "reports_accross_betas_SVM_G.pkl",
               "reports_accross_betas_SVM_ALL.pkl",
               "reports_accross_grades_SVM.pkl",
               "reports_accross_grades_MLR.pkl"]
OUT_PARAMS = ['train_accuracy',
              'test_recall',
              'train_recall',
              'train_precision',
              'test_accuracy',
              'test_precision',
              'train_f1',
              'test_f1']


"""
Classifier cross-validation parameters for evaluation
"""
CV_ITERS = 10


"""
Desired beta factors (weight penalization)
"""
BETAS = {
    #"SVM": [.99 + i/100.0 for i in xrange(0, 45, 5)] + [i/1000.0 for i in range(990, 1101, 1)],
    "SVM": [.98 + i/1000.0 for i in range(1, 40, 1)],
    "MLR": [i/10.0 for i in range(1, 36)]}
AUTO = ['auto']