import diagnose

if __name__ == '__main__':
    diagnose.runPerformanceMetrics(datasetId='ALL')
    diagnose.runPerformanceMetrics()
    diagnose.runROCPlotters()
    diagnose.createLatexTables()