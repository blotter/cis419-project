import matplotlib.pyplot as plt
from matplotlib.backends.backend_pdf import PdfPages
from collections import Counter
from sklearn.metrics import accuracy_score, precision_score, recall_score, roc_curve, auc, classification_report, confusion_matrix
import constants
import pickle
import util
import numpy as np

import os.path


"""
Author: Ege Sagduyu, Benedikt Lotter
Abstract class for the classifier implementations
"""


class Classifier():

    def __init__(self, datasetID, modelId):
        self.datasetID = datasetID
        self.modelId = modelId


    def tuneClassifier(self, X, y, iter=50):
        raise NotImplementedError()


    def fitWithClassWeights(self, X, y, m, classWeights):
        m.set_params(**{'clf__class_weight': classWeights})
        m.fit(X, y)
        return m


    def tuneAndPredictAllWeights(self, X, y, all_ids):
        """
        :param X: contains id column
        :param y:
        :return:
        """
        beta = constants.BETAS[self.modelId]
        X_train_bal, y_train_bal, train_ids, X_test, y_test, test_ids = util.trainTestSplitXandY(X, y, all_ids)
        tuneResult = self.tuneClassifier(X_train_bal, y_train_bal)
        m = tuneResult.best_estimator_
        print "tuned params: ", tuneResult.best_params_
        print "best score: ", tuneResult.best_score_

        results = {}
        for b in beta:
            print "cross-validation for ", self.modelId, self.datasetID, " for beta: ", b
            p = dict()
            p[0] = b
            p[1] = 1.0
            all_y_train_actual = []
            all_y_test_actual = []
            all_test_ids = []
            all_y_train_pred = []
            all_y_test_pred = []
            all_train_ids = []
            all_train_probs = []
            all_test_probs = []

            for k in range(constants.CV_ITERS):
                # Create new train, test split
                X_train, y_train_actual, train_ids, X_test, y_test_actual, test_ids = util.trainTestSplitXandY(X, y, all_ids)
                #print "np.mean(X_train)", np.mean(X_train, axis = 0), "np.std(X_train)", np.std(X_train, axis = 0)
                #print "np.mean(X_test)", np.mean(X_test, axis = 0), "np.std(X_test)", np.std(X_test, axis = 0)
                all_y_train_actual.append(y_train_actual)
                all_y_test_actual.append(y_test_actual)

                # Fit Classifier
                fitted_model = self.fitWithClassWeights(X_train, y_train_actual, m, p)

                # Generate Predictions
                y_train_pred = fitted_model.predict(X_train)
                y_test_pred = fitted_model.predict(X_test)
                # Attach results of this trial
                all_y_train_pred.append(y_train_pred)
                all_y_test_pred.append(y_test_pred)
                all_train_ids.append(train_ids)
                all_test_ids.append(test_ids)
                #print fitted_model.decision_function(X_test)
                if self.modelId == 'MLR':
                    all_test_probs.append(fitted_model.predict_proba(X_test))
                else:
                    all_test_probs.append(fitted_model.decision_function(X_test))

            results_for_beta = {"train" : {}, "test" : {} }
            results_for_beta["train"]["actual"] = all_y_train_actual
            results_for_beta["train"]["pred"] = all_y_train_pred
            results_for_beta["train"]["ids"] = all_train_ids
            results_for_beta["test"]["actual"] = all_y_test_actual
            results_for_beta["test"]["pred"] = all_y_test_pred
            results_for_beta["test"]["ids"] = all_test_ids
            results_for_beta["test"]["probs"] = all_test_probs
            results[b] = results_for_beta

        self.reportPerformance(results=results, baseFileName=constants.CV_RESULTS_BASE_FN)
        #Save only the tuned model, not the fitted model so that the model can be re-used with different train/test split
        #TODO: this model was actually already fitted on the initial data split
        self.saveModel(m)
        return results


    def diagnoseModel(self, Xtrain, ytrain, Xtest, ytest, m):
        """
        Generate various diagnostics about model
        :param Xtrain:
        :param ytrain:
        :param Xtest:
        :param ytest:
        :param m:
        :return: dictionary of classification metrics & print out
        """
        print "diagnosing model on training and testing performance"
        results = {}


        yPredTrain = m.predict(Xtrain)
        yPredTest = m.predict(Xtest)

        results["train_size"] = Xtrain.shape[0]
        results["train_accuracy"] = accuracy_score(ytrain, yPredTrain)
        results["train_precision"] = precision_score(ytrain, yPredTrain)
        results["train_recall"] = recall_score(ytrain, yPredTrain)
        results["train_classification_report"] = classification_report(ytrain, yPredTrain)
        results["train_confusion_matrix"] = confusion_matrix(ytrain, yPredTrain)

        results["test_size"] = Xtest.shape[0]
        results["test_accuracy"] = accuracy_score(ytest, yPredTest)
        results["test_precision"] = precision_score(ytest, yPredTest)
        results["test_recall"] = recall_score(ytest, yPredTest)
        results["test_classification_report"] = classification_report(ytest, yPredTest)
        results["test_confusion_matrix"] = confusion_matrix(ytest, yPredTest)
        results["ytest"] = ytest
        results["ypred"] = yPredTest

        results["model_params"] = m.get_params()

        #results = self.rocPlot(m, Xtest, ytest, results)
        print "results:\n"
        for k in results.keys():
            print '{}: \n{}\n'.format(k, results[k])
        return results


    def rocPlot(self, m, Xtest, ytest, results, modelId = "SVM", outFile = constants.SVM_ROC):
        """
        Create tpr/fps & auc for test predicitions from classifier
        :param m:
        :param Xtest:
        :param ytest:
        :param results:
        :param modelId:
        :param outFile:
        :return:
        """

        #TODO: adapt decision_function / predict_proba based on modelId
        if self.modelId == "SVM":
            fpr, tpr, _ = roc_curve(ytest, m.decision_function(Xtest))

        roc_auc = auc(fpr, tpr)

        results["fpr"] = fpr
        results["tpr"] = tpr
        results["roc_auc"] = roc_auc

        #set up plot
        pp = PdfPages(outFile)
        plt.figure()


        plt.plot([0, 1], [0, 1], 'k--')
        plt.xlim([0.0, 1.0])
        plt.ylim([0.0, 1.05])
        plt.xlabel('False Positive Rate')
        plt.ylabel('True Positive Rate')
        plt.title('ROC Naive Bayes <> SVM')


        #plot SVM classifier ROC curves for all classes
        plt.plot(results["fpr"], results["tpr"], label = "SVM " + "- AUC = %0.2f)" % results["roc_auc"])

        plt.legend(loc="lower right")
        #plt.show()
        plt.savefig(pp, format = "pdf")

        #close out ploting file
        pp.close()

        return results

    def reportPerformance(self, results, baseFileName):
        """
        Given the results, write them out to
        :param results:
        :return:
        """
        fName = "{}{}_{}".format(constants.RESULTS_DIR, baseFileName, self.modelId)
        if self.datasetID:
            fName = "{}_{}.pkl".format(fName, self.datasetID)
        with open(fName,'wb') as f:
            pickle.dump(results, f)


    def saveModel(self, m):
        """
        Save this model
        :param m:
        :return:
        """
        if self.datasetID:
            fName = "{}{}_{}_{}.{}".format(constants.MODELS_DIR, constants.MODEL_BASE_FN, self.modelId, self.datasetID, "pkl")
        with open(fName,'wb') as f:
            pickle.dump(m, f)









