"""
Author: Benedikt Lotter
Regression Model to predict recovery rates for defaulted loans
"""

import constants, util
import numpy as np
from sklearn.linear_model import LinearRegression
import pickle
from scipy import stats
import scipy as sc

class RecoveryModel():

    def __init__(self):
        """
        Initialize the model with pandas data frame
        :return:
        """
        print "Initializing regression model for recovery rates"
        X, y, all_ids = util.loadData(modelType=constants.MODEL_TYPES[1], pre_process_fun=processDataForRecovRegr)
        self.X_train, self.y_train, self.train_ids, self.X_test, self.y_test, self.test_ids = util.trainTestSplitXandY(X=X,y=y, all_ids=all_ids, balanceTrain=False)
        self.lm = LinearRegression()


    def fitRegression(self):
        """
        :return:
        """
        print "fitting regression model"
        self.lm = self.lm.fit(self.X_train, self.y_train)

    def reportPerformance(self):
        """
        Report performance of regression model
        :return:
        """
        results = {}
        results["y_test_pred"] = self.y_test
        results["y_test_pred"] = self.lm.predict(self.X_test)
        results["r_squared"] = self.lm.score(self.X_test, self.y_test)
        results["coef"] = self.lm.coef_
        results["intercept"] = self.lm.intercept_
        results["params"] = self.lm.get_params()
        with open(constants.INDEX_TO_X_VAR_MAP ,'rb') as f:
            varToIndex = pickle.load(f)

        varToCoef = dict(zip(varToIndex.keys(), results["coef"]))
        results["varToCoef"] = varToCoef
        results["coefPValue"] = dict(zip(varToIndex.keys(), self.getXVarCoefs()))
        return results


    def getXVarCoefs(self):
        x = self.X_test
        y = self.y_test
        n, k = x.shape
        yHat = np.matrix(self.lm.predict(x)).T

        # Change X and Y into numpy matricies. x also has a column of ones added to it.
        x = np.hstack((np.ones((n,1)),np.matrix(x)))
        y = np.matrix(y).T

        # Degrees of freedom.
        df = float(n-k-1)

        # Sample variance.
        sse = np.sum(np.square(yHat - y),axis=0)
        self.sampleVariance = sse/df

        # Sample variance for x.
        self.sampleVarianceX = x.T*x

        # Covariance Matrix = [(s^2)(X'X)^-1]^0.5. (sqrtm = matrix square root.  ugly)
        self.covarianceMatrix = sc.linalg.sqrtm(self.sampleVariance[0,0]*self.sampleVarianceX.I)

        # Standard erros for the difference coefficients: the diagonal elements of the covariance matrix.
        self.se = self.covarianceMatrix.diagonal()[1:]

        # T statistic for each beta.
        self.betasTStat = np.zeros(len(self.se))
        for i in xrange(len(self.se)):
            self.betasTStat[i] = self.lm.coef_[i]/self.se[i]

        # P-value for each beta. This is a two sided t-test, since the betas can be
        # positive or negative.
        self.betasPValue = 1 - stats.t.cdf(abs(self.betasTStat),df)

        return self.betasPValue




def processDataForRecovRegr(df):
    """
    Preprocessing function for recovery rate regression
    :param df:
    :return:
    """
    df = calcRecoveredPrncp(df)
    df = filterDefaults(df)
    return df



def calcRecoveredPrncp(df):
    """
    Add columns indidicating the $ & % of principal recovered
    :return:
    """
    print "data columns:"
    print df.columns
    df['outstd_prncp'] = df['loan_amnt'] - df['total_rec_prncp']
    df['recovery_rate'] = (df['loan_amnt'] - df['outstd_prncp']) / df['loan_amnt']
    return df


def filterDefaults(df):
    """
    Filter to only the defaulted loans
    :param df:
    :return:
    """
    dflt_msk = df['loan_status'].isin(constants.BAD_STATUS)
    dflt_loans = df[dflt_msk]
    return dflt_loans



