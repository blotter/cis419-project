"""
Pooled <> Composite Model Comparison
"""
import util, constants
from sklearn.metrics import accuracy_score, precision_score, recall_score, roc_curve, auc, classification_report, confusion_matrix
import sys, pickle
from svm import SVM
from mlr import MLR

def comparePooledToGradeModel(modelId, g, pooled_m):
    """
    :param g:
    :param pooled_m:
    :return:
    """
    p = dict()
    p[0] = 1.0
    p[1] = 1.0
    with open(util.getModelFileName(modelId, g),'rb') as f:
        grade_m = pickle.load(f)

    X, y, grade_ids = util.loadData(fname=util.fileNameForParams(g))

    X_train, y_train, train_ids, X_test, y_test, test_ids = util.trainTestSplitXandY(X, y, grade_ids)

    if modelId == "SVM":
        grade_svm = SVM(g, modelId="SVM")
        grade_svm.fitWithClassWeights(X_train, y_train, grade_m, p)

    elif modelId == "MLR":
        grade_mlr = MLR(g, modelId="MLR")
        grade_mlr.fitWithClassWeights(X_train, y_train, grade_m, p)

    y_test_pred_gm = grade_m.predict(X_test)
    y_test_precision_gm = precision_score(y_test, y_test_pred_gm)
    y_test_pred_pm = pooled_m.predict(X_test)
    y_test_precision_pm = precision_score(y_test, y_test_pred_pm)

    results = {}
    results["grade_m"] = {"test_precision:" : y_test_precision_gm}
    results["pooled_m"] = {"test_precision:" : y_test_precision_pm}
    return results


def compareModelsOnSubsets(modelId):
    """
    :param modelId:
    :return:
    """
    p = dict()
    p[0] = 1.0
    p[1] = 1.0

    with open(util.getModelFileName(modelId, constants.ALL_GRADES),'rb') as f:
        pooled_m = pickle.load(f)

    X_train, y_train, train_ids, X_test, y_test, test_ids = util.getTrainTestSplit()

    if modelId == "SVM":
        svm = SVM(constants.ALL_GRADES, modelId="SVM")
        svm.fitWithClassWeights(X_train, y_train, pooled_m, p)

    elif modelId == "MLR":
        mlr = MLR(constants.ALL_GRADES, modelId="MLR")
        mlr.fitWithClassWeights(X_train, y_train, pooled_m, p)

    comp_results = {}
    for g in constants.GRADES:
        comp_results[g] = comparePooledToGradeModel(modelId, g, pooled_m)

    return comp_results


if __name__ == '__main__':
    args = sys.argv
    modelId = args[1]
    x = compareModelsOnSubsets(modelId)
    print x