import pandas as pd
import numpy as np
from datetime import datetime
from pandas.tseries.offsets import *
from multiprocessing import Pool
import sys
from os import path
import util, constants
# TODO:
# - how to discretize date?
# - text columns: purpose


GOOD_STATUS = constants.GOOD_STATUS
BAD_STATUS = constants.BAD_STATUS
DONE_STATUS = constants.DONE_STATUS

PERCENTAGE_COLUMNS = constants.PERCENTAGE_COLUMNS

COLUMNS_TO_DROP = constants.COLUMNS_TO_DROP
NON_NA_COLUMNS = constants.NON_NA_COLUMNS

DISCRETE_COLUMNS = constants.DISCRETE_COLUMNS


def load_one(filename):
    """

    :param filename:
    :return:
    """
    df = pd.read_csv(filename, skiprows=[0])
    df['id'] = df['id'].convert_objects(convert_numeric=True)
    return df.dropna(how='all')

def pp_adjust_income(loans_data, filename=constants.RPP_DATA):
    """

    :param loans_data:
    :param filename:
    :return:
    """
    rpp_data = pd.read_csv(filename)
    df = loans_data.merge(rpp_data, left_on='addr_state', right_on='State')
    for id in constants.RPP_ADJ_VARS:
        df[id] = df[id] / df['RPP']
    return df

def concat_files(filenames):
    """

    :param filenames:
    :return:
    """
    p = Pool(len(filenames))
    dfs = p.map(load_one, filenames)
    p.close()

    return pd.concat(dfs)


def safe_string(s):
    """

    :param s:
    :return:
    """
    s = str(s)
    return s.strip().replace(' ', '_')


def add_discrete_columns(df, columns):
    """

    :param df:
    :param columns:
    :return:
    """
    for col in columns:
        for val in sorted(df[col].unique()):
            df['%s_%s' % (col, safe_string(val))] = (df[col] == val)*1

    return df


def fix_dates(df):
    """

    :param df:
    :return:
    """
    day_month = pd.DatetimeIndex(pd.to_datetime(df['issue_d'], coerce=True)) + MonthEnd(n=0)
    df['issue_date'] = pd.to_datetime(day_month)

    return df


def get_tbills(filename):
    """

    :param filename:
    :return:
    """
    tbill_date_parser = lambda x: datetime.strptime(x, '%d%b%Y')

    tbills = pd.read_table(filename, sep='\s+', skiprows=[0, 1, 2, 3], parse_dates=[0], date_parser=tbill_date_parser)[['date', 'Y3', 'Y5']]
    tbills = tbills.rename(columns={'date': 'tbill_date', 'Y3': 'tbill_3', 'Y5': 'tbill_5'})
    tbills.loc[:, 'tbill_3':'tbill_5'] /= 100

    return tbills


def add_tbills(df, tbills_df):
    """

    :param df:
    :param tbills_df:
    :return:
    """
    df = df.merge(tbills_df, left_on='issue_date', right_on='tbill_date').drop('tbill_date', 1)

    short_term_indexer = df.term_binary == 0
    long_term_indexer = df.term_binary == 1

    df['tbill_term_matched'] = np.nan
    df.loc[short_term_indexer, 'tbill_term_matched'] = df.loc[short_term_indexer, 'tbill_3']
    df.loc[long_term_indexer, 'tbill_term_matched'] = df.loc[long_term_indexer, 'tbill_5']
    df['int_premium'] = df['int_rate']-df['tbill_term_matched']

    return df


def add_base_rate(df):
    """

    :param df:
    :return:
    """
    a1_msk = df.sub_grade == 'A1'
    base_rate = df[a1_msk].groupby('issue_date')['int_rate'].mean().fillna(method='ffill')
    base_rate = base_rate.reset_index().rename(columns={'int_rate': 'base_rate'})
    df = df.merge(base_rate, on='issue_date')
    df['int_premium'] = df['int_rate'] - df['base_rate']

    return df


def fix_term(df):
    """

    :param df:
    :return:
    """
    df['term'] = df['term'].map(lambda x: x.strip())
    short_term_indexer = df['term'] == '36 months'
    long_term_indexer = df['term'] == '60 months'

    df['term_binary'] = np.nan
    df.loc[short_term_indexer, 'term_binary'] = 0
    df.loc[long_term_indexer, 'term_binary'] = 1

    return df


def add_loan_volume(df):
    """

    :param df:
    :return:
    """
    dollar_volume_grade = df.groupby(['issue_date', 'grade'])['loan_amnt'].sum()
    dollar_volume_grade.name = 'loan_issue_volume_grade'
    dollar_volume_total = dollar_volume_grade.unstack().sum(1)
    dollar_volume_total.name = 'loan_issue_volume'

    dvg_mean = pd.rolling_mean(dollar_volume_grade.unstack(), window=6, min_periods=1)
    dvg_mean = dvg_mean.fillna(method='ffill').stack()
    dvg_dev = (dollar_volume_grade-dvg_mean)/dollar_volume_grade
    dvg_dev.name = 'loan_issue_volume_grade_dev'

    dvt_mean = pd.rolling_mean(dollar_volume_total, window=6, min_periods=1)
    dvt_mean = dvt_mean.fillna(method='ffill')
    dvt_dev = (dollar_volume_total-dvt_mean)/dollar_volume_total
    dvt_dev.name = 'loan_issue_volume_dev'

    df = df.join(dollar_volume_grade, on=['issue_date', 'grade'])
    df = df.join(dvg_dev, on=['issue_date', 'grade'])
    df = df.join(dollar_volume_total, on=['issue_date'])
    df = df.join(dvt_dev, on=['issue_date'])

    return df


def add_fico_mid(df):
    """

    :param df:
    :return:
    """
    df['fico_mid'] = (df['fico_range_high']+df['fico_range_low'])/2.0

    return df


def add_grade_nums(df):
    """

    :param df:
    :return:
    """
    grade_map = {k: v for v, k in enumerate(sorted(df.grade.unique()))}
    df['grade_num'] = df['grade'].map(grade_map)

    return df


def add_sub_grade_nums(df):
    """

    :param df:
    :return:
    """
    sub_grade_map = {k: v for v, k in enumerate(sorted(df.sub_grade.unique()))}
    df['sub_grade_num'] = df['sub_grade'].map(sub_grade_map)

    return df


def add_emp_length_nums(df):
    """

    :param df:
    :return:
    """
    emp_length_map = {'%d years' % i: i for i in range(2, 10)}
    emp_length_map['1 year'] = 1
    emp_length_map['10+ years'] = 10
    emp_length_map['n/a'] = np.nan
    emp_length_map['< 1 year'] = 0

    df['emp_length_num'] = df['emp_length'].map(emp_length_map)

    return df.dropna(subset=['emp_length_num'])


def add_loan_status_binary(df):
    """

    :param df:
    :return:
    """
    good_indexer = df['loan_status'].isin(GOOD_STATUS)
    bad_indexer = df['loan_status'].isin(BAD_STATUS)
    known_indexer = good_indexer | bad_indexer

    df['loan_status_binary'] = np.nan
    df.loc[good_indexer, 'loan_status_binary'] = 1
    df.loc[bad_indexer, 'loan_status_binary'] = 0

    return df.dropna(subset=['loan_status_binary'])


def fix_percent(df, cols):
    """

    :param df:
    :param cols:
    :return:
    """
    def is_number(x):
        try:
            float(x)
            return True
        except ValueError:
            return False

    def p2f(x):
        if is_number(x):
            return x
        return float(x.strip('%'))/100

    for c in cols:
        if c in df:
            df[c] = df[c].map(p2f)

    return df


def concat_and_clean(filenames):
    """

    :param filenames:
    :return:
    """
    print "Loading data..."
    data = concat_files(filenames)
    # tbills = get_tbills(tbills_filename)

    print "Fixing dates..."
    data = fix_dates(data)

    print "Fixing percentage columns..."
    data = fix_percent(data, PERCENTAGE_COLUMNS)

    # print "Adding T-Bills..."
    # data = fix_term(data)
    # data = add_tbills(data, tbills)

    print "Adding derived features..."
    for c in data.columns:
        print c
    data = add_loan_volume(data)
    data = add_fico_mid(data)
    # data = add_grade_nums(data)
    # data = add_sub_grade_nums(data)
    data = pp_adjust_income(data)

    # TODO: should we just discretize employment length too?
    # technically, it shouldn't be converted to a number as it is a choice
    # of discrete buckets, and >10 encompasses many lengths other than 10
    data = add_emp_length_nums(data)
    data = add_loan_status_binary(data)
    data = add_base_rate(data)

    safe_drop = [c for c in COLUMNS_TO_DROP if c in data.columns]
    safe_non_na = [c for c in NON_NA_COLUMNS if c in data.columns]

    print "Discretizing..."
    data = add_discrete_columns(data, DISCRETE_COLUMNS)
    data = data.drop(DISCRETE_COLUMNS, 1)

    print "Writing..."
    data = data.drop(safe_drop, 1).dropna(subset=safe_non_na)

    return data


def stratify_loans(df):
    """
    
    :param df:
    :return:
    """
    grades = sorted(df.grade.unique())

    loans_grade = {}

    for g in grades:
        loans_grade[g] = df[df.grade == g]

    return loans_grade


def main(argv):
    filenames = argv[:-1]
    # tbills_filename = argv[-2]
    out_folder = argv[-1]

    data = concat_and_clean(filenames)

    data.to_pickle(path.join(out_folder, "loans_%s.pkl" % constants.ALL_GRADES))

    for g, lg in stratify_loans(data).items():
        fn = path.join(out_folder, "loans_%s.pkl" % g)
        lg.to_pickle(fn)

if __name__ == '__main__':
    main(sys.argv[1:])
