"""
Creates all the average metrics
Author: Ege Sagduyu
"""
from sklearn.metrics import accuracy_score, precision_score, recall_score, f1_score, roc_curve, auc, precision_recall_curve, average_precision_score, classification_report, confusion_matrix
import numpy as np
import pandas as pd
import pickle
import constants
from random import randint
import matplotlib.pyplot as plt
from collections import Counter
from matplotlib.backends.backend_pdf import PdfPages


def getRightFilenames(str, indir=constants.OUT_RES_BAL_PKL):
    """
    finds filenames that contain the given string
    :param str:
    :return:
    """
    relevant_names = []
    for name in indir:
        outdir = constants.RESULTS_DIR
        if indir == constants.OUT_REPORTS:
            outdir = constants.REPORTS_DIR
        if str in name:
            relevant_names += [outdir + name]
    return relevant_names


def pretty(d, indent=0):
    """
    pretty-prints nested dictionaries (adapted from StackOverlow for debugging)
    :param d:
    :param indent:
    :return:
    """
    for key, value in d.iteritems():
        print '\t' * indent + str(key)
        if isinstance(value, dict):
            pretty(value, indent+1)
        else:
            print '\t' * (indent+1) + str(value)


def getDiagnostics(ytrain, yPredTrain, ytest, yPredTest):
    """
    calculates the diagnostics for given pairs of train and test prediction vs actual values
    :param ytrain:
    :param yPredTrain:
    :param ytest:
    :param yPredTest:
    :return:
    """
    results = dict()
    results["train_accuracy"] = accuracy_score(ytrain, yPredTrain)
    results["train_precision"] = precision_score(ytrain, yPredTrain)
    results["train_recall"] = recall_score(ytrain, yPredTrain)
    results["train_f1"] = f1_score(ytrain, yPredTrain)

    results["test_accuracy"] = accuracy_score(ytest, yPredTest)
    results["test_precision"] = precision_score(ytest, yPredTest)
    results["test_recall"] = recall_score(ytest, yPredTest)
    results["test_f1"] = f1_score(ytest, yPredTest)
    return results

def concatVectors(data_dict):
    """
    helper function that performs the concats on a sub-dictionary (i.e, train, test etc.)
    :param mode:
    :param data_dict:
    :return:
    """
    y_actual = np.array([])
    y_pred = np.array([])
    for i in xrange(0,constants.CV_ITERS):
        y_actual = np.append(y_actual, data_dict['actual'][i])
        y_pred = np.append(y_pred, data_dict['pred'][i])
        temp = precision_score(y_actual, y_pred)
        print "\tFor fold {}, precision score is: {}".format(i, temp)
        if temp == 0.0 or temp == 0.5 or temp == 1.0:
            print y_actual
            print y_pred
    return y_actual, y_pred


def createMetricsAcrossBetasPerClf(clf, grade):
    """
    create the relevant metrics for a given classifier and grade
    :param clf:
    :return:
    """
    files = getRightFilenames(clf.upper() + '_' + grade.upper() + '.')
    print files
    pkls = dict()
    with open(files[0], 'rb') as f2:
        pkls = pickle.load(f2)
    output = dict()
    train = np.array([])
    test = np.array([])
    train_pred = np.array([])
    test_pred = np.array([])
    for beta in constants.BETAS[clf.upper()]:
        inst = pkls[beta]
        print "\nFor clf:{} and beta:{},".format(clf, beta)
        ytrain, yPredTrain = concatVectors(inst['train'])
        ytest, yPredTest = concatVectors(inst['test'])
        train = np.append(train, ytrain)
        test = np.append(test, ytest)
        train_pred = np.append(train_pred, yPredTrain)
        test_pred = np.append(test_pred, yPredTest)
        output[beta] = getDiagnostics(ytrain, yPredTrain, ytest, yPredTest)
    output['all_betas'] = getDiagnostics(train, train_pred, test, test_pred)
    fname = constants.REPORTS_DIR + "reports_accross_betas_{}_{}.pkl".format(clf, grade)
    with open(fname, 'wb') as f3:
        pickle.dump(output, f3)
    return output


def createMetricsAcrossBetasAllClf(grade):
    """
    computes the averages per grade for all classifiers
    :param grade:
    :return:
    """
    d = {}
    for clf in constants.CLASSIFIERS:
        d[clf] = createMetricsAcrossBetasPerClf(clf.upper(), grade)
    #pretty(d)
    return d


def createMetricsAcrossBetasForEveryGrade():
    """
    automates the above process for all grades
    :return:
    """
    for grade in constants.GRADES:
        print "\nFor grade {}:\n".format(grade)
        d = createMetricsAcrossBetasAllClf(grade)
        #pretty(d)
    return d


def createMetricsAcrossGradesPerClf(clf):
    """
    create the average precision across all grades for every beta value
    :param clf:
    :return:
    """
    files = getRightFilenames(clf)
    pkls = []
    for f in files:
        with open(f, 'rb') as f2:
            pkls.append(pickle.load(f2))
    output = dict()
    for beta in constants.BETAS[clf.upper()]:
        train = np.array([])
        test = np.array([])
        train_pred = np.array([])
        test_pred = np.array([])
        for pkl in pkls:
            inst = pkl[beta]
            ytrain, yPredTrain = concatVectors(inst['train'])
            ytest, yPredTest = concatVectors(inst['test'])
            train = np.append(train, ytrain)
            test = np.append(test, ytest)
            train_pred = np.append(train_pred, yPredTrain)
            test_pred = np.append(test_pred, yPredTest)
        output[beta] = getDiagnostics(train, train_pred, test, test_pred)
    fname = constants.REPORTS_DIR + "reports_accross_grades_{}.pkl".format(clf)
    with open(fname, 'wb') as f3:
        pickle.dump(output, f3)
    #pretty(output)
    return output

def createMetricsAcrossGradesAllClf():
    """
    automates the above process for all classifiers
    :return:
    """
    for clf in constants.CLASSIFIERS:
        print "\nFor {} classifier:\n".format(clf.upper())
        d = createMetricsAcrossGradesPerClf(clf.upper())
        #pretty(d)
    return d


def plotROCcurve(tuples_for_betas, params, start_index=0):
    """
    plots the ROC curve and saves it appropriately
    :param Xtrue:
    :param Xprobs:
    :param params:
    :return:
    """
    g = ''
    tag = "ROC Curves Across Grades"
    if params['grade']:
        g = '_' + params['grade']
        tag = 'ROC Curves Across Betas, Grade={}'.format(params['grade'])

    outFile = constants.ROC_DIR + 'roc_{}{}_{}.png'.format(params['clf'], g, str(start_index))
    output = dict()
    for i in xrange(start_index, len(constants.BETAS[params['clf']]), 4):
        beta = constants.BETAS[params['clf']][i]
        results = dict()
        fpr, tpr, _ = roc_curve(tuples_for_betas[beta][0], tuples_for_betas[beta][1])
        roc_auc = auc(fpr, tpr)
        plt.plot(fpr, tpr, lw=1, label='%s, Beta=%0.4f, (area = %0.4f)' % (params['clf'], beta, roc_auc))

        results["fpr"] = fpr
        results["tpr"] = tpr
        results["roc_auc"] = roc_auc
        output[beta] = results

    plt.plot([0, 1], [0, 1], 'k--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.title(tag)
    plt.legend(loc="lower right", fontsize='xx-small')
    plt.savefig(outFile)
    plt.close()
    return output

def plotPRcurve(tuples_for_betas, params, start_index=0):
    """
    plots the ROC curve and saves it appropriately
    :param Xtrue:
    :param Xprobs:
    :param params:
    :return:
    """
    g = ''
    tag = "Precision-Recall Curves Across Grades"
    if params['grade']:
        g = '_' + params['grade']
        tag = 'Precision-Recall Curves Across Betas, Grade={}'.format(params['grade'])

    outFile = constants.ROC_DIR + 'pr_{}{}_{}.png'.format(params['clf'], g, str(start_index))
    output = dict()
    for i in xrange(start_index, len(constants.BETAS[params['clf']]), 4):
        beta = constants.BETAS[params['clf']][i]
        results = dict()
        precision, recall, _ = precision_recall_curve(tuples_for_betas[beta][0], tuples_for_betas[beta][1])
        avg_precision = average_precision_score(tuples_for_betas[beta][0], tuples_for_betas[beta][1])
        plt.plot(precision, recall, lw=1, label='%s, Beta=%0.4f, (area = %0.4f)' % (params['clf'], beta, avg_precision))

        results["precision"] = precision
        results["recall"] = recall
        results["average_precision"] = avg_precision
        output[beta] = results

    plt.plot([0, 1], [0, 1], 'k--')
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('Recall')
    plt.ylabel('Precision')
    plt.title(tag)
    plt.legend(loc="upper left", fontsize='xx-small')
    plt.savefig(outFile)
    plt.close()
    return output

def createROCPlots(clf, grade=None):
    """
    creates the plot for a given specification, from one sample given at random
    :param mode: 'train' or 'test'
    :param target: 'betas' or 'grades'
    :param grade:
    :param clf:
    :return:
    """
    k = randint(0, (constants.CV_ITERS - 1))
    output = dict()
    params = {'clf':clf, 'grade':grade}

    if grade:
        files = getRightFilenames(clf.upper() + '_' + grade.upper() + '.')
        with open(files[0], 'rb') as f2:
            pkls = pickle.load(f2)
        train = np.array([])
        probs = np.array([])
        for beta in constants.BETAS[clf.upper()]:
            inst = pkls[beta]['test']
            yprobs = []
            ytrain = inst['actual'][k]
            if clf == 'MLR':
                yprobs = inst['probs'][k][:, 1]
            else:
                yprobs = inst['probs'][k]
            train = np.append(train, ytrain)
            probs = np.append(probs, yprobs)
            output[beta] = (ytrain, yprobs)
        output['all_betas'] = (train, probs)
    else:
        files = getRightFilenames(clf)
        pkls = []
        for f in files:
            with open(f, 'rb') as f2:
                pkls.append(pickle.load(f2))
        for beta in constants.BETAS[clf.upper()]:
            train = np.array([])
            probs = np.array([])
            for pkl in pkls:
                inst = pkl[beta]['test']
                yprobs = inst['probs'][k]
                ytrain = inst['actual'][k]
                if clf == 'MLR':
                    yprobs = inst['probs'][k][:, 1]
                print ytrain
                print yprobs
                train = np.append(train, ytrain)
                probs = np.append(probs, yprobs)
            output[beta] = (train, probs)
    res_roc0 = plotROCcurve(output, params)
    res_roc1 = plotROCcurve(output, params, start_index=1)
    res_roc2 = plotROCcurve(output, params, start_index=2)
    res_roc3 = plotROCcurve(output, params, start_index=3)
    res_pr0 = plotPRcurve(output, params)
    res_pr1 = plotPRcurve(output, params, start_index=1)
    res_pr2 = plotPRcurve(output, params, start_index=2)
    res_pr3 = plotPRcurve(output, params, start_index=3)
    return output


def createROCforAllClf(grade=None):
    """
    creates all the relevant ROC files by all classifiers
    :param grade:
    :return:
    """
    d = dict()
    for clf in constants.CLASSIFIERS:
        if grade:
            d['test']= createROCPlots(clf)
        else:
            temp = dict()
            temp['across_grades'] = createROCPlots(clf)
            for g in constants.GRADES_CONCAT:
                temp['test']= createROCPlots(clf, grade=g)
                d[g] = temp
    #pretty(d)
    return d


def createTables(clf, metric):
    """
    obtains the desired performance metric per classifier and creates a Dataframe table
    which it then outputs into LaTeX
    :param clf:
    :param metric:
    :return:
    """
    files = getRightFilenames(clf.upper() + '_', indir=constants.OUT_REPORTS)
    grades = constants.GRADES_CONCAT
    pkls = dict()
    for f in files:
        tag = ''
        for g in grades:
            if g + '.' in f:
                tag = g
        with open(f, 'rb') as f2:
            pkls[tag] = (pickle.load(f2))
    df = pd.DataFrame(index=constants.BETAS[clf.upper()], columns=grades)
    for key in pkls.keys():
        for beta in constants.BETAS[clf.upper()]:
            df.loc[beta, key] = pkls[key][beta][metric]
    # adding the composite metrics column
    comp_file = getRightFilenames(clf.upper() + '.', indir=constants.OUT_REPORTS)
    p = []
    with open(comp_file[0], 'rb') as f:
        p.append(pickle.load(f))
    s = []
    for beta in constants.BETAS[clf.upper()]:
        s.append(p[0][beta][metric])
    df['COMPOSITE'] = pd.Series(data=s, index=df.index)
    fName = constants.TABLES_DIR + 'table_{}_{}.pkl'.format(metric, clf)
    with open(fName, 'wb') as t:
        pickle.dump(df, t)
    print df
    return df


def createLatexTables():
    """
    runs the above script for all classifiers and metrics
    :return:
    """
    for clf in constants.CLASSIFIERS:
        for metric in constants.OUT_PARAMS:
            print '\n\nFor clf:{} and metric:{}, the latex table is:\n'.format(clf,metric)
            df = createTables(clf, metric)
            latex_output = df.to_latex()
            fname = constants.PLOT_DIR + 'latex_{}_{}.txt'.format(clf, metric)
            with open(fname, 'w') as f:
                f.write(latex_output)
            print latex_output
    return

def runPerformanceMetrics(datasetId=None):
    """
    creates result reports based on the defined grade (or all grades if there is no input)
    :param datasetId:
    :return:
    """
    if datasetId:
        createMetricsAcrossBetasAllClf(datasetId)
    else:
        createMetricsAcrossBetasForEveryGrade()
        createMetricsAcrossGradesAllClf()


def runROCPlotters(datasetId=None):
    """
    creates ROC curves based on the defined grade (or all grades if there is no input)
    :param datasetId:
    :return:
    """
    if datasetId:
        createROCforAllClf(grade=datasetId)
    else:
        createROCforAllClf()