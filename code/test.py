import util
import sys

if __name__ == '__main__':
    args = sys.argv
    if len(args) == 1:
        util.runPooledClassifiers()
    elif len(args) == 2:
        util.runClassifiersOnAllDatasets(args[1].strip())
