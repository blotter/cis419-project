import util
import sys

if __name__ == '__main__':
    args = sys.argv
    modelId = args[1]
    if args[2] == "p":
        util.predictTestWithClassifier(modelId, pooled=True)
    elif args[2] == "g":
        util.predictTestWithClassifier(modelId, pooled=False)
