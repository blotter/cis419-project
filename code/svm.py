"""
Author: Benedikt Lotter
SVM classifier, hyper-parameter optimization and related code
"""


from sklearn import svm, datasets, grid_search
from sklearn.pipeline import Pipeline
from sklearn.kernel_approximation import RBFSampler, Nystroem
from sklearn.svm import LinearSVC, SVC
from sklearn.linear_model import SGDClassifier
from classifier import Classifier
import numpy as np
import util, constants
import sys, traceback

class SVM(Classifier):


    def tuneClassifier(self, X, y, iter=50):
        """

        :param X:
        :param y:
        :param iter:
        :return:
        """
        pipeline = Pipeline([
            ('rbf', Nystroem()),
            ('clf', LinearSVC(dual=False)),
        ])

        cValues = [(2.0)**(i) for i in range(-25, 5, 2)]
        gammaValues = cValues

        params = {
            'rbf__gamma': gammaValues,
            'clf__C': cValues
        }

        tm = grid_search.GridSearchCV(pipeline, params, verbose=1)

        tm.fit(X, y)

        return tm


    def predictDefaultProbs(self, X, m):
        """
        Abstract method, whose model-specific implementation will use predict_proba or decision_function
        :param X:
        :param m:
        :return: Single column vector of probability of being a default loan
        """
        confd_scores = m.decision_function(X)
        return confd_scores

