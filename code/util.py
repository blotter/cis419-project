"""
Author: Benedikt Lotter
Utility Functions used across project
"""

import pandas as pd
import random
import numpy as np
import constants
import multiprocessing
import sys, traceback
import svm, mlr
from svm import SVM
from mlr import MLR
from sets import Set
import pickle
import os.path

"""
Data loading & pre-processing
"""


def loadData(fname=constants.DATA_DIR + constants.ALL_DATA, modelType = "DEFAULT_CLASSIFY", pre_process_fun = None):
    """
    Based on specified file name, load data into X & y data arrays with appropriate pre-processing
    :param fname:
    :return: X, y numpy array tuple
    """
    data = pd.read_pickle(fname)

    if pre_process_fun:
        data = pre_process_fun(data)

    all_ids = data["id"]

    # extract X, y numpy arrays from pandas data frame
    vars = getVarsForModel(data, modelType=modelType)
    yVars = vars["y"]
    y = data.as_matrix(columns=(yVars,))
    y = y.reshape(y.shape[0])
    dataX = data.drop(yVars, 1)

    xVars = Set(vars["X"])
    xVarsInData = xVars.intersection(dataX.columns)
    X = dataX.as_matrix(columns=xVarsInData)

    columIndexMap(xVarsInData)

    return (X, y, all_ids)


def columIndexMap(xVars):
    """
    :param xVars:
    :return:
    """
    ncol = len(xVars)
    colIx = list(xrange(ncol))
    indexToName = dict(zip(xVars, colIx))
    with open(constants.INDEX_TO_X_VAR_MAP,'wb') as f:
        pickle.dump(indexToName, f)
    return indexToName


def getVarsForModel(data, modelType=constants.MODEL_TYPES[0]):
    """
    Given the specific model, return a dictionary specifying the variable names used in the model
    :param modelType:
    :param data:
    :return:
    """
    #TODO: switch case on modelId (SVM vs. MLR) in case chosen variables should differ
    vars = {}
    if modelType == "DEFAULT_CLASSIFY":
        catVars = []
        for cVar in constants.X_VARS_CAT:
            catVars.extend([c for c in data.columns if cVar in c])

        xVars = catVars + constants.X_VARS_NUM
        vars["X"] = xVars
        vars["y"] = constants.Y_VAR_CLASSIFY


    elif modelType == "RECOV_REGR":
        catVars = []
        for cVar in constants.X_VARS_CAT:
            catVars.extend([c for c in data.columns if cVar in c])


        xVars = catVars + constants.X_VARS_NUM
        vars["X"] = xVars
        vars["y"] = constants.Y_VAR_RECOV_REGR

    return vars


def balanceDataFrame(df):
    """
    Create a Y-variable balanced subset file of the original file and output it
    :param df
    :return:
    """

    pos_msk = df[constants.Y_VAR_CLASSIFY] == 1
    neg_msk = df[constants.Y_VAR_CLASSIFY] == 0
    neg_count = neg_msk.sum()
    pos = df[pos_msk].sample(neg_count)

    return df[neg_msk].append(pos).reset_index()


def balanceXandY(X, y, ids):
    """
    Balance the sample (both in np array X and y)
    :param X:
    :param y:
    :param ids:
    :return:
    """
    pos_msk = y == 1
    pos_idx = np.where(pos_msk == True)[0]

    neg_msk = y == 0
    neg_idx = np.where(neg_msk == True)[0]
    neg_count = neg_msk.sum()

    pos_idx_sample = np.random.choice(pos_idx, neg_count, replace=False)
    all_bal_idx = np.concatenate((neg_idx, pos_idx_sample,))
    X_subset = X[all_bal_idx]
    y_subset = y[all_bal_idx]
    ids_subset = ids.iloc[all_bal_idx]

    return X_subset, y_subset, ids_subset


def subGradesForGrade(g):
    return [g.append(str(i)) for i in range(1, 6)]


def anyNaInfinite(arr):
    """
    return True if any values are Na/NaN/infinite, otherwise false
    :param arr:
    :return:
    """
    n, d = arr.shape
    for i in range(d):
        if (np.any(np.isnan(arr[:, i])) or not np.all(np.isfinite(arr[:, i]))):
            print "column", i, " - NA/infinite: ", (np.any(np.isnan(arr[:, i])) or not np.all(np.isfinite(arr[:, i])))
            print arr[:, i]
    return (np.any(np.isnan(arr)) or not np.all(np.isfinite(arr)))


def checkPdNa(df):
    """
    return True if any values in pandas data frame columnsare Na/NaN/infinite, otherwise false
    :param df:
    :return:
    """
    for c in df.columns:
        if df[c].isnull().values.any():
            print "column ", c, " - NA/infinite:", df[c].isnull().values.any()
    return df.isnull().values.any()


def normNumFeature(df, means=None, sds=None, normVars=constants.X_VARS_NUM):
    """
    Standard-normalize numerical features and return the means & standard deviations of each
    """
    print "variables to normalizes: ", normVars

    for v in normVars:
        if v not in df.columns:
            print "NORM VAR (%s) NOT IN DF" % v

    if (means is None and sds is None):
        means = df[normVars].mean()
        sds = df[normVars].std()

    df[normVars] = (df[normVars] - means) / sds

    return (df, means, sds,)


def normNumFeaturesInX(X, means=None, sds=None, normVars=constants.X_VARS_NUM):
    """
    :param X:
    :param means:
    :param sds:
    :return:
    """

    with open(constants.INDEX_TO_X_VAR_MAP ,'rb') as f:
        varToIndex = pickle.load(f)

    if (means is None and sds is None):
        means = {}
        sds = {}

    for v in normVars:
        idx = varToIndex[v]

        if not (v in means):
            means[v] = X[:, idx].mean()
            sds[v] = X[:, idx].std()

        X[:, idx] = (X[:, idx] - means[v]) / sds[v]

    return (X, means, sds, )



def processRawData(fname, blc=True, split=0.8):
    """
    Create normalized data file
    """
    print "pre-processing raw data"

    fname_prefix = fname[:fname.rfind('.')]
    df = pd.read_pickle(fname)

    print "creating train/test split"
    msk = np.random.rand(len(df)) < split
    train = df.loc[msk, :]
    test = df.loc[~msk, :]

    print "normalizing train and test data"
    train, means, sds = normNumFeature(train)
    print "did train"
    test, means, sds = normNumFeature(test, means=means, sds=sds)

    print "writing out normalized train & test data"
    train.to_pickle('%s_train.pkl' % fname_prefix)
    test.to_pickle('%s_test.pkl' % fname_prefix)

    if blc:
        print "creating balanced train & test set"
        trainB = balanceDataFrame(train)
        testB = balanceDataFrame(test)
        trainB.to_pickle('%s_train_bal.pkl' % fname_prefix)
        testB.to_pickle('%s_test_bal.pkl' % fname_prefix)





def trainTestSplitXandY(X, y, all_ids, split = 0.8, balanceTrain = True):
    """
    Split both X and y into train and test sample
    :param X:
    :param y:
    :param split:
    :param balanceTrain:
    :return:
    """
    n, d = X.shape
    msk = np.random.rand(n) < split
    train_ids = all_ids[msk]

    test_ids = all_ids[~msk]
    X_train = X[msk]
    y_train = y[msk]
    if balanceTrain:
        X_train, y_train, train_ids = balanceXandY(X_train, y_train, train_ids)

    X_train, means, sds = normNumFeaturesInX(X_train)
    X_test = X[~msk]
    y_test = y[~msk]


    #print "number of training instances: ", len(train_ids)
    #print "number of test instances: ", len(test_ids)


    X_test, means, sds = normNumFeaturesInX(X_test, means, sds)

    return X_train, y_train, train_ids, X_test, y_test, test_ids



if __name__ == "__main__":
    if len(sys.argv) < 2:
        sys.exit(1)

    for f in sys.argv[1:]:
        processRawData(f)


def fileNameForParams(grade=None, train=False, test=False, bal=False):
    fName = "{}loans".format(constants.DATA_DIR)
    if grade:
        fName = "{}_{}".format(fName, grade)
    if train:
        fName = "{}_{}".format(fName, "train")
    if test:
        fName = "{}_{}".format(fName, "test")
    if bal:
        fName = "{}_{}".format(fName, "bal")

    fName += ".pkl"

    return fName


def runPooledClassifier():
    """
    Run classifiers in parallel on the datasets of the different grades
    :return:
    """

    X, y, all_ids = loadData(fname=fileNameForParams(grade=constants.ALL_GRADES))
    svm_result, mlr_result = runClassifiersOnDataset(X, y, all_ids, datasetId=constants.ALL_GRADES)
    return svm_result, mlr_result

def runClassifiersOnGrades(grades = constants.GRADES):
    """
    collect results for svm and mlr on all grades
    :param grades:
    :return:
    """
    svm_results = []
    mlr_results = []
    for g in grades:
        print "runClassifierOnAllDatasets on g: ", g
        svm_result, mlr_result = runClassifiersOnAllDatasets(g)
        svm_results.append(svm_result)
        mlr_results.append(mlr_result)

    #print "svm_results: ", svm_results
    #print "mlr_results: ", mlr_results
    return svm_results, mlr_results

def runClassifiersOnAllDatasets(g):
    """
    For a certain grade, run the classifiers
    :param g:
    :return:
    """
    #print "running classifier on dataset of grade", g
    X, y, all_ids = loadData(fname=fileNameForParams(grade=g))

    try:
        svm_result, mlr_result = runClassifiersOnDataset(X, y, all_ids, datasetId=g)
    except:
        print "bad news"
        raise Exception("".join(traceback.format_exception(*sys.exc_info())))

    return svm_result, mlr_result


def runClassifiersOnDataset(X, y, all_ids, datasetId):
    """
    Train, predict and report performance for classifiers on specified dataset
    """
    svm = SVM(datasetId, "SVM")
    mlr = MLR(datasetId, "MLR")


    svm_result = svm.tuneAndPredictAllWeights(X, y, all_ids)

    mlr_result = mlr.tuneAndPredictAllWeights(X, y, all_ids)

    return svm_result, mlr_result


def getModelFileName(modelId, datasetId):
    """
    """
    if datasetId:
        fName = "{}{}_{}_{}.{}".format(constants.MODELS_DIR, constants.MODEL_BASE_FN, modelId, datasetId, "pkl")

    return fName


def getResultsFileName(modelId, pooled):
    """
    """
    pooled_or_by_grade = "pooled" if pooled else "by_grade"
    fName = "{}{}_{}_{}.{}".format(constants.RESULTS_DIR, constants.PREDICTIONS_BASE_FN, modelId, pooled_or_by_grade, "pkl")
    return fName


def fixTrainTestSplit():
    """
    :return:
    """

    X, y, all_ids = loadData()
    # Split data into test and train
    split_dict = createSplitDict(X, y, all_ids)

    fName = "{}{}.{}".format(constants.DATA_DIR, constants.TRAIN_TEST_SPLIT, "pkl")
    with open(fName,'wb') as f:
        pickle.dump(split_dict, f)


def createSplitDict(X, y, all_ids):
    """

    :param X:
    :param y:
    :param all_ids:
    :return:
    """
    X_train_bal, y_train_bal, train_ids, X_test, y_test, test_ids = trainTestSplitXandY(X, y, all_ids)

    split_dict = {}
    split_dict["X_train_bal"] = X_train_bal
    split_dict["y_train_bal"] = y_train_bal
    split_dict["train_ids"] = train_ids
    split_dict["X_test"] = X_test
    split_dict["y_test"] = y_test
    split_dict["test_ids"] = test_ids

    return split_dict


def getTrainTestSplit():
    """
    :return:
    """
    fName = "{}{}.{}".format(constants.DATA_DIR, constants.TRAIN_TEST_SPLIT, "pkl")
    if not os.path.isfile(fName):
        fixTrainTestSplit()

    with open(fName,'rb') as f:
        split_dict = pickle.load(f)

    X_train_bal = split_dict["X_train_bal"]
    y_train_bal = split_dict["y_train_bal"]
    train_ids = split_dict["train_ids"]
    X_test = split_dict["X_test"]
    y_test = split_dict["y_test"]
    test_ids = split_dict["test_ids"]


    return X_train_bal, y_train_bal, train_ids, X_test, y_test, test_ids


def getTrainTestSplitForGrade(grade):
    """
    :param grade:
    :return:
    """
    X_train_bal, y_train_bal, train_ids, X_test, y_test, test_ids = getTrainTestSplit()

def predictTestWithClassifier(modelId, pooled = False):

    #data = pd.read_pickle(fname)

    if pooled:
        # Get Pooled Model
        modelFn = getModelFileName(modelId, constants.ALL_GRADES)
        print modelFn
        with open(getModelFileName(modelId, constants.ALL_GRADES),'rb') as f:
            m = pickle.load(f)

        # Split data into test and train
        X_train_bal, y_train_bal, train_ids, X_test, y_test, test_ids = getTrainTestSplit()

        print "X_train_bal.shape", X_train_bal.shape
        print "y_train_bal.shape", y_train_bal.shape
        print "X_test.shape", X_test.shape
        print "y_test.shape", y_test.shape

        # Fit model on balanced train data for each class weight
        results_per_b = yTestResultsPerBeta(modelId=modelId, datasetId = constants.ALL_GRADES, m=m, X_train=X_train_bal, y_train=y_train_bal, train_ids=train_ids, X_test=X_test, y_test=y_test, test_ids=test_ids)


        fName = getResultsFileName(modelId=modelId, pooled=True)
        with open(fName,'wb') as f:
            pickle.dump(results_per_b, f)

    else:
        X_train_bal, y_train_bal, train_ids, X_test, y_test, test_ids = getTrainTestSplit()
        results_by_grade = {}
        for g in constants.GRADES:
            with open(getModelFileName(modelId, datasetId=g),'rb') as f:
                m = pickle.load(f)
            X_train_grade, y_train_grade, train_ids_grade = filterXandYToGrade(X_train_bal, y_train_bal, train_ids, g)
            X_test_grade, y_test_grade, test_ids_grade = filterXandYToGrade(X_test, y_test, test_ids, g)
            print "X_test_grade.shape", X_test_grade.shape
            results_per_b = yTestResultsPerBeta(modelId=modelId, datasetId = g, m=m, X_train=X_train_grade, y_train=y_train_grade, train_ids=train_ids_grade, X_test=X_test_grade, y_test=y_test_grade, test_ids=test_ids_grade)
            results_by_grade[g] = results_per_b
        results_per_b = chainGradePredictions(modelId, results_by_grade)
        fName = getResultsFileName(modelId=modelId, pooled=False)
        with open(fName,'wb') as f:
            pickle.dump(results_per_b, f)

    return results_per_b



def chainGradePredictions(modelId, results_by_grade):
    """

    :param modelId:
    :param results_by_grade:
    :return:
    """
    results_per_b = {}
    beta = constants.BETAS[modelId]
    result_cols = constants.TEST_PRED_COLUMNS
    for b in beta:
        test_ids = tuple([results_by_grade[g][b][result_cols[0]] for g in constants.GRADES])
        y_test = tuple([results_by_grade[g][b][result_cols[1]] for g in constants.GRADES])
        y_test_pred = tuple([results_by_grade[g][b][result_cols[2]] for g in constants.GRADES])
        y_test_probs = tuple([results_by_grade[g][b][result_cols[3]] for g in constants.GRADES])

        y_test_result = pd.DataFrame(columns=result_cols)

        y_test_result[result_cols[0]] = pd.concat(test_ids)
        print [y.shape for y in y_test]
        y_test_result[result_cols[1]] = np.concatenate(y_test)
        y_test_result[result_cols[2]] = np.concatenate(y_test_pred)
        y_test_result[result_cols[3]] = np.concatenate(y_test_probs)

        results_per_b[b] = y_test_result

    return results_per_b



def yTestResultsPerBeta(modelId, datasetId, m, X_train, y_train, train_ids, X_test, y_test, test_ids):
    """
    :param modelId:
    :param m:
    :param X_train:
    :param y_train:
    :param train_ids:
    :param X_test:
    :param y_test:
    :param test_ids:
    :return:
    """
    results_per_b = {}
    beta = constants.BETAS[modelId]
    for b in beta:
        p = dict()
        p[0] = b
        p[1] = 1.0

        if modelId == "SVM":
            svm = SVM(datasetId, modelId="SVM")
            svm.fitWithClassWeights(X_train, y_train, m, p)
            y_test_pred = m.predict(X_test)
            y_test_probs = svm.predictDefaultProbs(X_test, m)

        elif modelId == "MLR":
            mlr = MLR(datasetId, modelId="MLR")
            mlr.fitWithClassWeights(X_train, y_train, m, p)
            y_test_pred = m.predict(X_test)
            y_test_probs = mlr.predictDefaultProbs(X_test, m)



        # Column 1: lending club ids of test instances
        # Column 2: actual y values of test instances
        # Column 3: predicted y values of test instances
        # Column 4: probabilities of being a negative (default) instance
        result_cols = constants.TEST_PRED_COLUMNS
        y_test_result = pd.DataFrame(columns=result_cols)
        y_test_result[result_cols[0]] = test_ids
        y_test_result[result_cols[1]] = y_test
        y_test_result[result_cols[2]] = y_test_pred
        y_test_result[result_cols[3]] = y_test_probs

        # Add result pandas df to results_per_w
        results_per_b[b] = y_test_result

    return results_per_b



def filterMaker(grade):
    def filter_on_grade(df):
        return func(df, grade)
    return filter_on_grade


def filterXandYToGrade(X, y, idx, grade):
    """
    :param df:
    :param grade:
    :return:
    """
    with open(constants.INDEX_TO_X_VAR_MAP ,'rb') as f:
        varToIndex = pickle.load(f)

    inds = []
    for i in range(1,6):
        sg = 'sub_grade_%s%i' % (grade, i)
        inds.append(varToIndex[sg])
    #start_ind = varToIndex['sub_grade_%s1' % grade]
    grade_msk = np.any(X[:, inds], 1)
    X = X[grade_msk]
    y = y[grade_msk]
    idx = idx[grade_msk]
    return X, y, idx


def filterDfToGrade(df, grade):
    """
    :param df:
    :param grade:
    :return:
    """
    grade_msk = df['grade'] == grade
    return df['grade']
