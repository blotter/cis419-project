import util
import sys
import diagnose
import constants

if __name__ == '__main__':
    args = sys.argv
    if args[1] == "p":
        util.runPooledClassifier()
    elif args[1] == "g":
        if len(args) > 2:
            util.runClassifiersOnGrades(args[2:])
        else:
            util.runClassifiersOnGrades()
