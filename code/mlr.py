"""
Author: Ege Sagduyu
Modified Linear Regression (using SGDClassifier)
Formatted like blotter's svm.py
"""


from sklearn import svm, datasets, grid_search
from sklearn.linear_model import SGDClassifier
from sklearn.linear_model import LogisticRegression
from classifier import Classifier
from sklearn.pipeline import Pipeline

import numpy as np
import util, constants

class MLR(Classifier):

    def tuneClassifier(self, X, y, iter=50):
        """

        :param X:
        :param y:
        :param iter:
        :return:
        """
        pipeline = Pipeline([
            ('clf', LogisticRegression())
            ])
        print "tuning MLR"

        cValues = [(2.0)**(i) for i in range(-15, 5, 2)]

        params = {'clf__C': cValues}
        print "gridsearch tuning parameters: ", params
        tm = grid_search.GridSearchCV(pipeline, params)
        tm.fit(X, y)
        print "finished tuning MLR --> params: ", tm.best_params_, ", score: ", tm.best_score_
        return tm


    def predictDefaultProbs(self, X, m):
        """
        Abstract method, whose model-specific implementation will use predict_proba or decision_function
        :param X:
        :param m:
        :return: Single column vector of probability of being a default loan
        """
        probs_by_class = m.predict_proba(X)
        return probs_by_class[:, 1]
