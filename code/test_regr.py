import recovery_model

if __name__ == '__main__':

    lm = recovery_model.RecoveryModel()
    lm.fitRegression()
    lm_results = lm.reportPerformance()
    print lm_results
